create table tb_article(
  id int primary key auto_increment,
  title varchar(200),
  description varchar(200),
  author varchar(200),
  imageurl varchar(200),
  created_date varchar(200),
  cat_id int,
);