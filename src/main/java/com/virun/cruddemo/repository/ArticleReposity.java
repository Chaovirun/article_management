package com.virun.cruddemo.repository;

import com.virun.cruddemo.model.Article;
import com.virun.cruddemo.model.Category;
import com.virun.cruddemo.provider.ArticleProvider;
import com.virun.cruddemo.provider.CategoryProvider;
import com.virun.cruddemo.util.Pagination;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;


public interface ArticleReposity {


    List<Article> findByName(String name);

    @SelectProvider(method = "findAll", type = ArticleProvider.class)
    @Results({
            @Result(property = "image", column = "imageurl"),
            @Result(property = "createdDate", column = "created_date")
    })
    Article findById(Category category, Integer id);
//    @SelectProvider(method = "findAll", type = ArticleProvider.class)
//    @Results({
//            @Result(property = "image", column = "imageurl"),
//            @Result(property = "createdDate", column = "created_date")
//    })
    List<Article> findAll();

    @SelectProvider(method = "insert", type = ArticleProvider.class)
    @Results({
            @Result(property = "image", column = "imageurl"),
            @Result(property = "createdDate", column = "created_date")
    })
    void save(@Param("article") Article article);

    @SelectProvider(method = "delete", type = ArticleProvider.class)
    void delete(Integer id);

    @SelectProvider(method = "update", type = ArticleProvider.class)
    void update(@Param("article") Article article);

    Integer getLastId();

    List<Article> getNextPage(int page);

    @SelectProvider(method = "getFilterPage", type = ArticleProvider.class)
    @Results({
            @Result(property = "image", column = "imageurl"),
            @Result(property = "createdDate", column = "created_date")
    })
    List<Article> getFilterPage(@Param("category") Category category,@Param("paging") Pagination pagination);

    @SelectProvider(method = "getCountAllFilter", type = ArticleProvider.class)
    Integer getCountAllFilter(@Param("cate") Category category);

    @SelectProvider(method = "getFilterCate", type = ArticleProvider.class)
    List<Article> getFilterCategory(String category);

    @SelectProvider(method = "viewArticle", type = ArticleProvider.class)
    @Results({
            @Result(property = "image", column = "imageurl"),
            @Result(property = "createdDate", column = "created_date")
    })
    Article viewArticle(Integer id);

}