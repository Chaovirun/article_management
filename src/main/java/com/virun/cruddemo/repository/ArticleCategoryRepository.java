package com.virun.cruddemo.repository;

import com.github.javafaker.Cat;
import com.virun.cruddemo.model.Category;
import com.virun.cruddemo.provider.CategoryProvider;
import com.virun.cruddemo.util.Pagination;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface ArticleCategoryRepository {

    @SelectProvider(method = "findAllCategories", type = CategoryProvider.class)
    List<Category> findAllCategories();

    @SelectProvider(method = "getAllCategories", type = CategoryProvider.class)
    List<Category> getAllCategories(@Param("paging")Pagination pagination);

    @SelectProvider(method = "findAllCategories", type = CategoryProvider.class)
    Category findCatById(Integer cat_id);

    @SelectProvider(method = "findNameCategory", type = CategoryProvider.class)
    Category findCatByName(String cat_title);

    @SelectProvider(method = "add", type = CategoryProvider.class)
    void addArticleCategory(@Param("category") Category category);

    @SelectProvider(method = "remove", type = CategoryProvider.class)
    void deleteArticleCategory(Integer id);

    @SelectProvider(method = "removeAll", type = CategoryProvider.class)
    void deleteArticleCategoryAll(Integer id);

    @SelectProvider(method = "update", type = CategoryProvider.class)
    void editArticleCategory(@Param("cate") Category category);

    @SelectProvider(method = "getCountArticleCategory", type = CategoryProvider.class)
    List<Integer> getCountArticleCategory(@Param("cate") Category category);

    @SelectProvider(method = "getCountCategory", type = CategoryProvider.class)
    List<Integer> getCountCategory();

}
