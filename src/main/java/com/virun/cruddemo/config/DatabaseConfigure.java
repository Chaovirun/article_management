package com.virun.cruddemo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfigure {

    @Bean
    @Profile("development")
    DataSource development(){
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        builder.setType(EmbeddedDatabaseType.H2);
        builder.addScript("script/create_table.sql");
        builder.addScript("script/create_tbcategory.sql");
        builder.addScript("script/insert_article.sql");
        builder.addScript("script/insert_category.sql");
        return builder.build();
    }

    @Bean
    @Profile("production")
    DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://localhost:5432/db_article");
        dataSource.setUsername("postgres");
        dataSource.setPassword("Virun021198");
        return dataSource;
    }

}
