package com.virun.cruddemo.controller;

import com.virun.cruddemo.model.Article;
import com.virun.cruddemo.model.Category;
import com.virun.cruddemo.service.ArticleCategoryService;
import com.virun.cruddemo.service.ArticleService;
import com.virun.cruddemo.util.Pagination;
import org.junit.experimental.theories.DataPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class ArticleController {

    private ArticleService articleService;
    private ArticleCategoryService categoryService;

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setCategoryService(ArticleCategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/")
    public String index(Model model, Category category, Pagination pagination){
        System.out.println(category+" | "+pagination);
        List<Category> categories = categoryService.findAllCategories();
        List<Integer> categoryList = categoryService.getCountArticleCategory(category);

        for (int i = 0; i < categoryList.size(); i++) {
            if (i<categories.size()){
                categories.get(i).setCount_cat(categoryList.get(i));
            }

        }

        List<Article> articleList = articleService.getFilterPage(category, pagination);
        model.addAttribute("categories", categories);
        model.addAttribute("articles", articleList);
        model.addAttribute("category", category);
        model.addAttribute("count", categoryList);
        model.addAttribute("paging", pagination);
        model.addAttribute("hasArticle", articleList.size());
        return "index";
    }

    @GetMapping("/detail/{id}")
    public String detail(ModelMap model, @PathVariable Integer id){
        model.addAttribute("article", articleService.viewArticle(id));
        model.addAttribute("categories",categoryService.findAllCategories());
        model.addAttribute("category", new Category());
        System.out.println("articleService.viewArticle(id) = "+articleService.viewArticle(id).toString());
        return "article-detail";
    }

    @GetMapping("/add")
    public String addArticle(ModelMap map){
        map.addAttribute("article", new Article());
        map.addAttribute("categories",categoryService.findAllCategories());
        map.addAttribute("category", new Category());
        System.out.println("AddArticle : GET ");
        return "add";
    }

    @PostMapping("/add")
    public String addArticles(ModelMap map,@RequestParam("file") MultipartFile file, @Valid @ModelAttribute Article article, BindingResult bindingResult){
        System.out.println("Add failed image : " + article.getImage());
        if (bindingResult.hasErrors()){
            map.addAttribute("article", article);
            System.out.println(article.toString());
            System.out.println("Add failed image : " + article.getImage() + " | " + article.getTitle()+ " | " + article.getAuthor());
            return "add";
        }
        if (!file.isEmpty()) {
            System.out.println(file.getOriginalFilename());
            try {
                if (Files.deleteIfExists(Paths.get(System.getProperty("user.dir") + "/src/main/resources/images/", file.getOriginalFilename()))) {
                    System.out.println("deleted existing file!!!");
                }
                Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir") + "/src/main/resources/images/", file.getOriginalFilename()));
                article.setImage(file.getOriginalFilename());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            article.setImage("default-thumnail.png");
        }
        System.out.println("Add : cat_id : " + article.getCat_id());
        articleService.save(article);
        System.out.println("ADD POST : " + article.getTitle() + "--image--" + article.getImage());
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Integer id){
        System.out.println("ID"+id);
        articleService.delete(id);
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String update(ModelMap model, @PathVariable Integer id){
        model.addAttribute("article", articleService.viewArticle(id));
        model.addAttribute("categories",categoryService.findAllCategories());
        model.addAttribute("category", new Category());
        System.out.println("Update GET = "+articleService.viewArticle(id).toString());
        return "update-user";
    }
    @PostMapping("/update")
    public String updateArticle(@RequestParam("id") Integer id,@RequestParam("image") String image, ModelMap map,@Valid @ModelAttribute Article article, @RequestParam("file") MultipartFile file, BindingResult result){
        if (result.hasErrors()){
            map.addAttribute("article", article);
            System.out.println("Upadate fialed "+article.toString());
            return "/update";
        }
        if (!file.isEmpty()){
            try {
                if (Files.deleteIfExists(Paths.get(System.getProperty("user.dir")+"/src/main/resources/images/", file.getOriginalFilename()))){
                    System.out.println("deleted existing file!!!");
                }
                Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir")+"/src/main/resources/images/", file.getOriginalFilename()));
                article.setImage(file.getOriginalFilename());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            article.setImage(articleService.viewArticle(id).getImage());
        }
        article.setId(id);
        System.out.println("Update ID : "+article.getId());
        articleService.update(article);
        System.out.println("update POST : " + article.getId() + "--image--" + article.getImage());
        return "redirect:/";
    }

    @GetMapping("/category")
    public String categotry(Model model,Pagination pagination){
        System.out.println(pagination.getLimit()+ " | "+pagination.getOffset());
        model.addAttribute("category", categoryService.getAllCategories(pagination));
        model.addAttribute("paging", pagination);
        return "category";
    }

    @GetMapping("/category/add-category")
    public String addCate(Model model){
        model.addAttribute("category", new Category());
        model.addAttribute("isAddCat", true);
        return "add-category";
    }

    @PostMapping("/category/add-category/save")
    public String addCateSave(@ModelAttribute Category category){
        categoryService.addArticleCategory(category);
        return "redirect:/category";
    }

    @GetMapping("/category/delete-cate/{id}")
    public String deleteCate(@PathVariable Integer id){
        categoryService.deleteArticleCategory(id);
        categoryService.deleteArticleCategoryAll(id);
        return "redirect:/category";
    }

    @GetMapping("/category/update-cate/{id}")
    public String updateCate(Model model,@PathVariable Integer id){
        model.addAttribute("category",categoryService.findCatById(id));
        model.addAttribute("isAddCat", false);
        System.out.println("GET UPDATE CATEGORY : " + categoryService.findCatById(id).toString());
        return "add-category";
    }

    @PostMapping("/category/update-cate/save")
    public String upateCateSave(@ModelAttribute Category category){
        categoryService.editArticleCategory(category);
        System.out.println("POST UPDATE CATEGORY : " + category.toString());
        return "redirect:/category";
    }
}