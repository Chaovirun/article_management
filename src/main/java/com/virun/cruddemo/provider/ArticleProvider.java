package com.virun.cruddemo.provider;

import com.github.javafaker.Cat;
import com.virun.cruddemo.model.Article;
import com.virun.cruddemo.model.Category;
import com.virun.cruddemo.util.Pagination;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

import java.util.Date;

public class ArticleProvider {
    public String findAll(Category category,Integer id){
        return new SQL(){{
            SELECT("id", "title", "description", "author", "imageurl", "created_date", "tb_category.cat_id", "tb_category.cat_title");
            FROM("tb_article");
            INNER_JOIN("tb_category ON tb_article.cat_id = tb_category.cat_id");
            if (category.getCat_id()!=null && id!=null){
                WHERE("tb_article.cat_id = "+category.getCat_id() + " AND tb_article.id = " + id);
            }
            ORDER_BY("tb_article.id");
        }}.toString();
    }
    public String delete(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_article");
            WHERE("id = " + id);
        }}.toString();
    }
    public String update(@Param("article") Article article){
        return new SQL(){{
            UPDATE("tb_article");
            SET("title = #{article.title}");
            SET("description = #{article.description}");
            SET("author = #{article.author}");
            if(article.getImage()!=null){
                SET("imageurl = #{article.image}");
            }else{
                SET("imageurl = default-thumnail.png");
            }
            SET("created_date = '"+new Date().toString()+"'");
            SET("cat_id = #{article.cat_id}");
            WHERE("id = #{article.id}");
        }}.toString();
    }
    public String insert(@Param("article") Article article){
        return new SQL(){{
            INSERT_INTO("tb_article");
            VALUES("title", "#{article.title}");
            VALUES("description", "#{article.description}");
            VALUES("author", "#{article.author}");
            VALUES("imageurl", "#{article.image}");
            VALUES("created_date", "'"+new Date().toString()+"'");
            VALUES("cat_id", "#{article.cat_id}");
        }}.toString();
    }

    public String getFilterPage(@Param("category") Category category,@Param("paging") Pagination paging){
        System.out.println("Pagination : "+paging.getLimit());
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.imageurl");
            SELECT("a.created_date, a.author, c.cat_id, c.cat_title");
            FROM("tb_article a");
            LEFT_OUTER_JOIN("tb_category c ON a.cat_id = c.cat_id");

            if(category.getCat_id()!=null)
                WHERE("a.cat_id = #{category.cat_id}");

            if(category.getCat_title()!=null)
                WHERE("a.title ILIKE '%' || #{category.cat_title} || '%'");

            ORDER_BY("a.id LIMIT #{paging.limit} OFFSET #{paging.offset}");

        }}.toString();
    }

    public String getCountAllFilter(@Param("cate") Category cate){
        return new SQL(){{
            SELECT("COUNT(A.id)");
            FROM("tb_article A");
            if(cate.getCat_id()!=null)
                WHERE("A.cat_id = #{cate.cat_id}");

            if(cate.getCat_title()!=null)
                WHERE("A.title ILIKE '%' || #{cate.cat_title} || '%'");
        }}.toString();
    }


    public String getFilterCate(String category){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.imageurl, a.created_date, c.cat_id, c.cat_title");
            FROM("tb_article AS a");
            INNER_JOIN("tb_category AS c ON a.cat_id = c.cat_id");
            WHERE("a.cat_title = " + category);
            ORDER_BY("a.id");
        }}.toString();
    }

    public String viewArticle(Integer id){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.imageurl, a.created_date, c.cat_id, c.cat_title");
            FROM("tb_article AS a");
            INNER_JOIN("tb_category AS c ON a.cat_id = c.cat_id");
            WHERE("a.id = " + id);
        }}.toString();
    }
}
