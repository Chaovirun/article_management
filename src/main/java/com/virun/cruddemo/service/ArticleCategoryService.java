package com.virun.cruddemo.service;

import com.virun.cruddemo.model.Category;
import com.virun.cruddemo.util.Pagination;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleCategoryService {
    List<Category> findAllCategories();
    Category findCatById(Integer cat_id);
    Category findCatByName(String cat_title);
    void addArticleCategory(Category category);
    void deleteArticleCategory(Integer id);
    void deleteArticleCategoryAll(Integer id);
    void editArticleCategory(Category category);
    List<Category> getAllCategories(Pagination pagination);

    List<Integer> getCountArticleCategory(Category category);

}
